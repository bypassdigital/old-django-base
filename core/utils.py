from itertools import count

from django.utils.text import slugify


def slug_generator(model, value, slug_field_name='slug', max_length=25):
    """
    A unique slug generation script for models. Only 'model' and 'value' arguments are requires.
    Args:
        model = Model class. If you use in 'save' method, you can use as 'self.__class__'
        value = The string that your slug is going to be based on
        slug_field_name = name of the slug field of model. Default is 'slug'
        max_length = Maximum lenght of your slug. Default is '25'

    Example usage with 'title' field as base field:
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = slug_generator(self.__class__, self.title)
            super().save(*args, **kwargs)
    """

    slug_candidate = slug_original = slugify(value, allow_unicode=True)

    for i in count(1):
        if not model.objects.filter(**{slug_field_name: slug_candidate}):
            return slug_candidate
        else:
            slug_candidate = '{}-{}'.format(slug_original, i)
