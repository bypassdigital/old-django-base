from django import template
from ..models import SiteSettings

register = template.Library()


@register.simple_tag()
def sitesettings():
    try:
        return SiteSettings.objects.first()
    except Exception:
        pass


@register.simple_tag
def get_name():
    try:
        if sitesettings().name:
            return sitesettings().name
        else:
            return "Deneme Site"
    except Exception:
        return "Deneme Site"


@register.simple_tag
def get_logo():
    try:
        if sitesettings().logo:
            return sitesettings().logo
        else:
            return None
    except Exception:
        return None


@register.simple_tag
def get_desc():
    try:
        if sitesettings().desc:
            return sitesettings().desc
        else:
            return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
    except Exception:
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"


@register.simple_tag
def get_email():
    try:
        if sitesettings().contact_email:
            return sitesettings().contact_email
        else:
            return "deneme@denememail.com"
    except Exception:
        return "deneme@denememail.com"


@register.simple_tag
def get_phone():
    try:
        if sitesettings().phone:
            return sitesettings().phone
        else:
            return "05555555555"
    except Exception:
        return "05555555555"


@register.simple_tag
def get_maps_api():
    try:
        if sitesettings().maps_api:
            return sitesettings().maps_api
        else:
            return "AIzaSyBMlLa3XrAmtemtf97Z2YpXwPLlimRK7Pk"
    except Exception:
        return "AIzaSyBMlLa3XrAmtemtf97Z2YpXwPLlimRK7Pk"


@register.simple_tag
def get_footer():
    try:
        if sitesettings().footer:
            return sitesettings().footer
        else:
            return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    except Exception:
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
                tempor incididunt ut labore et dolore magna aliqua."


@register.simple_tag
def get_address():
    try:
        if sitesettings().address:
            return sitesettings().address
        else:
            return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
    except Exception:
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
